export * from './p6.arduino';
export * from './p6.gui';
export * from './p6.osc';

import { rebar } from './p6.gui.js';

/** DOM might not be ready when we first execute this */
document.addEventListener("DOMContentLoaded", function(event) { 
  // add it to the body on init
  document.body.innerHTML += rebar.template;
  document.addEventListener('keyup', rebar.handleKeyUp );
});

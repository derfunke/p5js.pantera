export class MCU {

	constructor(config) {
		this.config = config;
		this.serial = null;
	}

	init() {
		// make an instance of the SerialPort object
		this.serial = new p5.SerialPort();
	
		// get a list of all the serial ports available
		// You should have a callback defined to see the results. See gotList, below:
		this.serial.list();
	
		// assuming the arduino is connected open a serial session on it
		this.serial.open(this.config.port, { 'baudRate' : this.config.baudRate });
	
		// When you get a list of serial ports that are available
		this.serial.on('list', onSerialList);
		this.serial.on('connected', onSerialConnected); // callback for connecting to the server
		this.serial.on('open', onSerialOpened);
		this.serial.on('error', onSerialError);
		this.serial.on('close', onSerialClosed);
	
		// When you some data from the serial port
		this.serial.on('data', onSerialData);
	}
	
	onSerialError(error) {
		console.log("(!!!) ERROR -> " + error);
	}
	
	// called when we receive the list of available serial ports
	onSerialList(thelist) {
		print("Available serial ports:");
		for (let i = 0; i < thelist.length; i++) {
			print(i + " " + thelist[i]);
		}
	}
	
	onSerialConnected() {
		console.log("bound to serial port");
	}
	
	onSerialOpened() {
		console.log("serial connection has been established");
	}
	
	onSerialClosed() {
		console.log("serial connection has been closed");
	}
	
	//  called when there is data available from the serial port
	onSerialData() {
		let currentString = serial.readLine();  // read the incoming data
		trim(currentString);                    // trim off trailing whitespace
		if (!currentString) return;             // if the incoming string is empty, do no more
		console.log(currentString);
	}

}

## 2 NOV 2019
This article helped a great deal in understanding how to use rollup-generated modules.
https://risanb.com/posts/bundling-your-javascript-library-with-rollup/#amd-output

## For Pantera developers

For Arduino support Pantera uses the p5js.serial library which is kind of inconsistenly documented, this is the most complete tutorial on how to use it.

https://itp.nyu.edu/physcomp/labs/labs-serial-communication/lab-serial-input-to-the-p5-js-ide/



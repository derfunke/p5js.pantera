module.exports = {
  input: 'src/core/pantera.js',
  output: {
    file: 'examples/build/pantera/pantera.bundle.js',
    name: 'P6',
    exports: 'named',
    format: 'es'
  }
};
